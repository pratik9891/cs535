'From VisualWorks® Personal Use Edition, 7.9.1 of 18 October 2012 on 8 September 2013 at 9:49:15 PM'!


!Core.Character methodsFor: 'accessing'!

next
	"Answer z if current char is Z, 
	a if current char is z. 
	Increments otherwise"

	| current |
	current := self asCharacter.
	current = $Z 
		ifTrue: [^$z].
	current = $z
		ifTrue: [^$a].
	^(self asInteger + 1) asCharacter! !