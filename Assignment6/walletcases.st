d := Dictionary new.
d at:'MXN' put: 100.
d at:'USD' put: 200
d at:'MXN' put: (d at:'MXN') + 100.
d includesKey:'USD'  
d do: [:each | each]
(10 asFixedPoint:2) negated -10.00s
o := OrderedCollection new
o add: '$10 USD' asCurrency.
o add: '$20 USD' asCurrency.
o add: '$1 MXN' asCurrency.
w := Wallet new initialize
w add: '$10 USD' asCurrency
w class = Wallet
'$10 USD' asCurrency negated 
sum := '$0.0 USD' asCurrency 
100 timesRepeat: [sum := sum + ('$0.19 USD' asCurrency)]
 c := '$10 USD' asCurrency -  '$10 USD' asCurrency.
d := '$10 USD' asCurrency +  '$10 USD' asCurrency.
c + d
test := '$1 USD' asCurrency + '$1 USD' asCurrency.
testCurrencyExchange := CurrencyExchange generate.
now := Timestamp now.
(test worthIn: 'USD' at: now - 40 days basedOn:testCurrencyExchange) = '$2.00 USD' asCurrency true
test := '$10 USD' asCurrency asWallet.
test worthIn: 'MXN' at: (Timestamp readFromDateAndTime: (ReadStream on: '11.15.2013 17:30')) basedOn:testCurrencyExchange