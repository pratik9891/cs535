<?xml version="1.0"?>

<st-source>
<time-stamp>From VisualWorks® Personal Use Edition, 7.9.1 of 18 October 2012 on 8 September 2013 at 8:45:09 PM</time-stamp>


<methods>
<class-id>Core.String</class-id> <category>accessing</category>

<body package="Collections-Text" selector="rotate:">rotate: shift	| len |	shift &gt; 0	ifTrue:[ ^self collect: [:each | len := each asInteger+shift.			len &gt; 122				ifTrue: [((len-122-1)+97) asCharacter]				ifFalse: [len asCharacter]]]	ifFalse:[^self collect: [:each | len := each asInteger+shift.			len &lt;97				ifTrue:[(122-(97-len-1)) asCharacter]				ifFalse:[len asCharacter]]]</body>
</methods>

</st-source>
