<?xml version="1.0"?>

<st-source>
<time-stamp>From VisualWorks® Personal Use Edition, 7.9.1 of 18 October 2012 on 8 September 2013 at 5:58:43 PM</time-stamp>


<methods>
<class-id>Core.Character</class-id> <category>accessing</category>

<body package="Magnitude-General" selector="next">next	"Answer z if current char is Z, 	a if current char is z. 	Increments otherwise"	| current |	current := self asCharacter.	current = $Z 		ifTrue: [^$z].	current = $z		ifTrue: [^$a].	^(self asInteger + 1) asCharacter</body>
</methods>

</st-source>
