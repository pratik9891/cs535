
b := BankAccount fromFile: 'test'. 
b balance $150.00 USD
b availableBalance $150.00 USD
b availableBalanceIn: (time + 2 days)  $10.00 USD
b account balanceIn: Timestamp now - 16 days
b availableBalance  ($-913.75) INR
b balance $20.00 USD
v := ValueWithHistory new.
[v valueAt: Timestamp now] on: Error do: [:exception |  ^'$1 USD' asCurrency]
5 days 
(time - 5 days) asDate asDays - ((Timestamp now) asDays) asDate

b := Bank routingNumber: '055012581'.
b transactionsFrom: 'test'.
b outgoingTransactions
(b account:'000032451294') balance   $520.00 USD
 (b account:'000032451294') balanceIn: (time + 3 days)  $520.00 USD
(b account:'000032451294') balance  $520.00 USD
 (b account:'000032451294') availableBalanceIn: (time + 4 days) $500.00 USD

b1 := BankAccount accountID: '12345678' 

t := CurrencyExchange generate
time := Timestamp readFromDateAndTime: (ReadStream on:'12.1.2013').
t exchangeRateOf: 'CAD' to: 'MXN' on: time + 5 days 12.3499
