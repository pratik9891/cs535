test := 'deposit	0.01'
read := test readStream.
read upTo: Character tab 'deposit'
read next
read upToEnd
read upToEnd 123
read reset
read nextLine. 
read nextLine 
read nextLine. 'a test.'
read lines OrderedCollection ('this' 'is ' 'a stream')

x := 1.201 asFixedPoint:2. 
y := 1.049999999999999999999 asFixedPoint:2.
z := x + y 
(x - 5) negative true
0.25674>0.25674

cent := '0.01' asNumber asFixedPoint:2.
sum := 0.0 asFixedPoint:2.
100000 timesRepeat:[sum := sum + cent].
1.23 asRational

name := 'data\Pratik.txt'.
filename := (String with: Filename separator), 'test' asFilename
filename directoryContents

(Filename named: '..',s,'.') directoryContents 

file := '../data/test1' asFilename.
file exists
fileRead := file readStream.
fileRead nextLine

a := BankAccount name: 'test'
a loadFromFile: 'test'
Character tab 

('0' asNumber) = ('x' asNumber ) 
'1,2.3,5' productSeparatedBy:$,  11.5
'1.2' asNumber +3 

x := OrderedCollection new
x add: '.'
x add: 'def'
x size 

test := ' a 	tab
	a space

	and special characters $562! #'.
T := 	test readStream 
c := T lines
c asArray #(' a 	tab' '	a space' '' '	and special characters $562! #')